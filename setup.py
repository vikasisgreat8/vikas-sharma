from distutils.core import setup

setup(
    name = "vikas-sharma",
    version = "0.9",
    packages = [
        "kong",
        "kong.management",
        "kong.management.commands",
        "kong.templatetags",
        "kong.tests",
    ],
    author = "Vikas Sharma",
    author_email = "vikasisgreat8@gmail.com",
    description = "A server description and deployment testing tool for King Kong sized sites",
    url = "https://bitbucket.org/vikasisgreat8/vikas-sharma/src/master/",
    package_data = {
        'kong': [
            'templates/*.html',
            'templates/kong/*.html',
            'templates/kong/*.txt',
            'fixtures/*.json',

        ],
    },
)
